using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLObjects;
using XPF;

namespace MXPF
{
	public class ConcatDS : DynamicStringBase
	{
		string str;

		public ConcatDS(XMLNode node) : base(node)
		{
			if(node.HasValue)
				throw new Exception($"CONCAT musn't have a value");
			StringBuilder sb = new StringBuilder();
			foreach(XMLNode n in node.GetSubNodes("s"))
				if(!n.HasValue)
					throw new Exception($"CONCAT-S must have a value");
				else
					sb.Append(n.Value);
			this.str = sb.ToString();
		}

		public override string GetString()
		{
			return this.str;
		}

		[Register]
		public static void Register(Managers m)
		{
			DynamicStringManager.ObjectCreator oc = (node, managers) => new ConcatDS(node);
			m.dm.Register("concat", oc);
			m.dm.Register("concatenate", oc);
		}
	}

	public class DateDS : DynamicStringBase
	{
		string format;
		bool utc;

		public DateDS(XMLNode node) : base(node)
		{
			if(node.HasValue || node.HasSubNodes)
				throw new Exception("DATE musn't have a value or subnodes");
			if(node.HasAttr("format"))
				this.format = node["format"];
			else
				this.format = "yyyy-MM-dd_HH-mm-ss";
			if(node.HasAttr("utc"))
			{
				string utcStr = node["utc"].ToLower();
				if(utcStr != "true" && utcStr != "false")
					throw new Exception("DATE-UTC must be TRUE or FALSE");
				this.utc = utcStr == "true";
			}
		}

		public override string GetString()
		{
			DateTime dt = this.utc ? DateTime.UtcNow : DateTime.Now;
			return dt.ToString(this.format);
		}

		[Register]
		public static void Register(Managers m)
		{
			m.dm.Register("date", (node, managers) => new DateDS(node));
		}
	}
}
