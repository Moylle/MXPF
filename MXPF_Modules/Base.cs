using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MXPF_Modules
{
	public static class Base
	{
		public static void Extract()
		{
			Console.WriteLine("Info: Extracting mxpf.xml to ./mxpf.xml");
			using(Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("MXPF.mxpf.xml"))
			using(StreamReader reader = new StreamReader(stream))
				System.IO.File.WriteAllText("./mxpf.xml", reader.ReadToEnd());
			Console.WriteLine("Info: mxpf.xml successfully extracted");
		}
	}
}
