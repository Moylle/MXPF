/* This minifier based on the code of Efficient stylesheet minifier
 * (http://madskristensen.net/post/Efficient-stylesheet-minification-in-C)
 */

/* Feb 28, 2010
 *
 * Copyright (c) 2010 Mads Kristensen (http://madskristensen.net)
 */

using System.Text;
using System.Text.RegularExpressions;

namespace MXPF
{
	public static class CSSMinifier
	{
		private static readonly Regex redundantIdSelectorRegex = new Regex(@"[a-zA-Z]+#");
		private static readonly Regex lineBreakRegex = new Regex(@"[\n\r]+\s*");
		private static readonly Regex multipleSpacesRegex = new Regex(@"\s+");
		private static readonly Regex separatingCharacters = new Regex(@"\s?([:,;{}])\s?");
		private static readonly Regex zeroValue = new Regex(@"([\s:]0)(px|%|em|in|cm|mm|ex|pt|pc|ch|rem|vh|vm|vmin|vmax|vw)");
		private static readonly Regex commentRegex = new Regex(@"/\*[\s\S]*?\*/");

		public static string Minify(string content)
		{
			if (string.IsNullOrWhiteSpace(content))
				return "";
			string newContent = content;
			newContent = multipleSpacesRegex.Replace(newContent, " ");
			newContent = separatingCharacters.Replace(newContent, "$1");
			newContent = zeroValue.Replace(newContent, "$1");
			newContent = commentRegex.Replace(newContent, string.Empty);
			return newContent;
		}
	}
}
