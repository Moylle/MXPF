/* This is a modification of a .NET port of the Douglas Crockford's JSMin 'C' project. *
 *
 * The author's copyright message is reproduced below.
 */

/* jsmin.c
   2013-03-29

Copyright (c) 2002 Douglas Crockford (www.crockford.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

The Software shall be used for Good, not Evil.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

using System;
using System.IO;
using System.Text;

namespace MXPF
{
	public sealed class JSMinifier
	{
		const double AVERAGE_COMPRESSION_RATIO = 0.6;
		const int EOF = -1;

		StringReader reader;
		StringWriter writer;
		int a;
		int b;
		int lookahead = EOF;
		int x = EOF;
		int y = EOF;

		public string Minify(string content)
		{
			lock(this)
			{
				a = 0;
				b = 0;
				lookahead = EOF;
				x = EOF;
				y = EOF;

				int estimatedCapacity = (int)Math.Floor(content.Length * AVERAGE_COMPRESSION_RATIO);
				StringBuilder sb = new StringBuilder(estimatedCapacity);
				content = content.Replace("\r\n", "\n");
				this.reader = new StringReader(content);
				this.writer = new StringWriter(sb);

				try
				{
					InnerMinify();
					this.writer.Flush();
					return sb.ToString().TrimStart();
				}
				catch(JSMinifyException)
				{
					throw;
				}
				finally
				{
					this.reader?.Close();
					this.writer?.Close();
				}
			}
		}

		private static bool IsAlphanum(int c)
		{
			return ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') ||
				(c >= 'A' && c <= 'Z') || c == '_' || c == '$' || c == '\\' ||
				c > 126);
		}

		private int Get()
		{
			int c = lookahead;
			lookahead = EOF;

			if(c == EOF)
				c = reader.Read();
			if(c >= ' ' || c == '\n' || c == EOF)
				return c;
			if (c == '\r')
				return '\n';
			return ' ';
		}

		private int Peek()
		{
			return lookahead = Get();
		}

		private int Next()
		{
			int c = Get();
			if (c == '/')
				switch (Peek())
				{
					case '/':
						while((c = Get()) > '\n')
							;
						break;
					case '*':
						Get();
						while (c != ' ')
							switch (Get())
							{
								case '*':
									if (Peek() == '/')
									{
										Get();
										c = ' ';
									}
									break;
								case EOF:
									throw new JSMinifyException("Unterminated comment.");
							}
						break;
				}
			y = x;
			x = c;
			return c;
		}

		private void Action(int d)
		{
			if (d == 1)
			{
				Put(a);
				if((y == '\n' || y == ' ') &&
					(a == '+' || a == '-' || a == '*' || a == '/') &&
					(b == '+' || b == '-' || b == '*' || b == '/'))
					Put(y);
			}

			if (d <= 2)
			{
				a = b;
				if (a == '\'' || a == '"' || a == '`')
					while(true)
					{
						Put(a);
						if ((a = Get()) == b)
							break;
						if (a == '\\')
						{
							Put(a);
							a = Get();
						}
						if (a == EOF)
							throw new JSMinifyException("Unterminated string literal.");
					}
			}

			if (d <= 3 && (b = Next()) == '/' && (
					a == '(' || a == ',' || a == '=' || a == ':' ||
					a == '[' || a == '!' || a == '&' || a == '|' ||
					a == '?' || a == '+' || a == '-' || a == '~' ||
					a == '*' || a == '/' || a == '{' || a == '\n'))
			{
				Put(a);
				if(a == '/' || a == '*')
					Put(' ');
				Put(b);
				while(true)
				{
					if((a = Get()) == '[')
						while(true)
						{
							Put(a);
							if((a = Get()) == ']')
								break;
							if(a == '\\')
							{
								Put(a);
								a = Get();
							}
							if(a == EOF)
								throw new JSMinifyException("Unterminated set in Regular Expression literal.");
						}
					else if(a == '/')
					{
						switch(Peek())
						{
							case '/':
							case '*':
								throw new JSMinifyException("Unterminated set in Regular Expression literal.");
						}
						break;
					}
					else if(a == '\\')
					{
						Put(a);
						a = Get();
					}
					if (a == EOF)
						throw new JSMinifyException("Unterminated Regular Expression literal.");
					Put(a);
				}
				b = Next();
			}
		}

		private void InnerMinify()
		{
			if (Peek() == 0xEF)
				for(int i = 0; i < 3; ++i)
					Get();
			a = '\n';
			Action(3);
			while (a != EOF)
				switch (a)
				{
					case ' ':
						Action(IsAlphanum(b) || (b == ':' && Peek() == ':') ? 1 : 2);
						break;
					case '\n':
						switch (b)
						{
							case '{':
							case '[':
							case '(':
							case '+':
							case '-':
							case '!':
							case '~':
								Action(1);
								break;
							case ' ':
								Action(3);
								break;
							default:
								Action(IsAlphanum(b) || (b == ':' && Peek() == ':') ? 1 : 2);
								break;
						}
						break;
					default:
						switch (b)
						{
							case ' ':
								Action(IsAlphanum(a) || a == ']' ? 1 : 3);
								break;
							case '\n':
								switch (a)
								{
									case '}':
									case ']':
									case ')':
									case '+':
									case '-':
									case '"':
									case '\'':
									case '`':
										Action(1);
										break;
									default:
										Action(IsAlphanum(a) ? 1 : 3);
										break;
								}
								break;
							default:
								Action(1);
								break;
						}
						break;
				}
		}

		private void Put(int c)
		{
			this.writer.Write((char)c);
		}
	}
	public class JSMinifyException : Exception
	{
		public JSMinifyException(string message) : base(message)
		{ }
	}
}
