using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MXPF
{
	public static class ExtendedString
	{
		public static List<string> ExtendedSplit(this string s, string[] seperators, bool seperateSeperators = true)
		{
			List<string> splitted = new List<string> { s, null };
			List<string> tmp;
			foreach(string seperator in seperators)
			{
				tmp = new List<string>();
				for(int i = 0; i < splitted.Count; i += 2)
				{
					foreach(string tmpSplit in splitted[i].Split(new string[] { seperator }, StringSplitOptions.None))
						if(seperateSeperators)
						{
							tmp.Add(tmpSplit);
							tmp.Add(seperator);
						}
						else
							tmp.Add(tmpSplit + seperator);
					tmp.RemoveAt(tmp.Count - 1);
					tmp.Add(splitted[i + 1]);
				}
			}
			return splitted;
		}
	}
}
