using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MXPF
{
	public static class Minifier
	{
		public static string MinifyCSS(string input)
		{
			return CSSMinifier.Minify(input);
		}
		public static string MinifyJS(string input)
		{
			return new JSMinifier().Minify(input);
		}
	}
}
