using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using XMLObjects;
using XPF;

namespace MXPF
{
	public class IsFileCondition : StandaloneCondition
	{
		public bool fileMode;

		public IsFileCondition(string id = null, string[] classes = null, bool? forcedState = null) : base(id, classes, forcedState)
		{ }

		public IsFileCondition(XMLNode node) : base(node)
		{
			if(node.Name == "isfile")
				this.fileMode = true;
		}

		protected override bool InternalTest(File f)
		{
			return this.fileMode == !f.isDirectory;
		}

		[Register]
		public static void Register(Managers m)
		{
			Manager<ICondition>.ObjectCreator oc = (node, managers) => new IsFileCondition(node);
			m.cm.Register("isfile", oc);
			m.cm.Register("isdir", oc);
			m.cm.Register("isdirectory", oc);
			m.cm.Register("isfolder", oc);
		}
	}

	public class ExtensionCondition : ConditionBase
	{
		public readonly string extension;

		public ExtensionCondition(string id = null) : base(id)
		{ }
		public ExtensionCondition(XMLNode node) : base(node)
		{
			if(!node.HasValue)
				if(!node.HasSubNodes)
					throw new Exception("EXTENSION mustn't have sub nodes");
				else
					this.extension = "";
			else
				this.extension = node.Value.ToLower();
		}

		protected override bool InternalTest(File f)
		{
			return f.Extension.ToLower() == this.extension;
		}

		[Register]
		public static void Register(Managers m)
		{
			m.cm.Register("extension", (node, manager) => new ExtensionCondition(node));
		}
	}

	public class TextCondition : ConditionBase
	{
		public enum SearchMode
		{
			PLAIN,
			REGEX
		}

		public enum SearchType
		{
			NAME,
			CONTENT,
			PATH
		}
		Matches matches = new Matches();

		public string text;
		public Regex r;
		public SearchMode searchMode = SearchMode.PLAIN;
		public Encoding encoding = Encoding.UTF8;
		public string[] lineSeperators;
		public SearchType searchType = SearchType.CONTENT;
		public RegexOptions regexOptions = RegexOptions.Compiled;
		public Predicate<string> lineMatcher;

		public TextCondition(string id = null) : base(id)
		{ }
		public TextCondition(XMLNode node) : base(node)
		{
			if(!node.HasValue)
				throw new Exception("TEXT must have a text");
			if(node.HasAttr("encoding"))
				this.encoding = Encoding.GetEncoding(node.Attributes["encoding"]);
			if(node.HasAttr("mode") && !Enum.TryParse(node.Attributes["mode"].ToUpper(), out this.searchMode))
				throw new Exception($"Invalid value '{node.Attributes["mode"]}' for MODE-Attribute. Allowed are PLAIN, REGEX");
			if(node.HasAttr("type") && !Enum.TryParse(node.Attributes["type"].ToUpper(), out this.searchType))
				throw new Exception($"Invalid value '{node.Attributes["type"]}' for TYPE-Attribute. Allowed are NAME, CONTENT, PATH");
			if(node.HasAttr("regexoptions"))
			{
				RegexOptions ro;
				foreach(string option in node["regexoptions"].Split(';'))
				{
					ro = default(RegexOptions);
					if(!Enum.TryParse(option, true, out ro))
						throw new Exception($"Invalid value '{option}' in REGEXOPTIONS-attribute. Allowed are only values from RegexOptions-enum");
					regexOptions |= ro;
				}
			}

			this.text = node.Value;
			if(this.searchMode == SearchMode.REGEX)
			{
				try
				{
					this.r = new Regex(this.text, this.regexOptions);
				}
				catch
				{
					throw new Exception("Error while creating Regex-Object. Maybe the regexoptions are mean to each other...");
				}
				this.lineMatcher = (s) => this.r.Match(s).Length != 0;
			}
			else
				this.lineMatcher = (s) => s.Contains(this.text);
		}

		protected override bool InternalTest(File f)
		{
			this.matches = new Matches();
			string matchString = null;
			switch(this.searchType)
			{
				case SearchType.CONTENT:
					if(f.isDirectory)
					{
						Console.WriteLine("Warning: Cannot apply CONTENT-Type to folder. Condition test failed");
						return false;
					}
					if(!f.HasTextContent)
						f.ReadText(this.encoding);
					matchString = f.TextContent;
					break;
				case SearchType.NAME:
					matchString = f.Name;
					break;
				case SearchType.PATH:
					matchString = f.RelativePath;
					break;
			}
			f.textContentChanged = false;
			return this.lineMatcher(matchString);
		}

		public override object GetReturnValue()
		{
			return this.matches;
		}

		[Register]
		public static void Register(Managers m)
		{
			m.cm.Register("text", (node, manager) => new TextCondition(node));
		}


		public class Matches : List<Match>
		{ }
		public class Match
		{
			public string text;
			public int start;
			public int length;
			public string matchedText;

			public Match(string text, int start, int length)
			{
				this.text = text;
				this.start = start;
				this.length = length;
				this.matchedText = text.Substring(start, length);
			}
		}
	}

	public class FoundLines
	{
		public string[] lineSeperators;
		public List<int> foundLines;
	}

	public class BinaryCondition : ConditionBase
	{
		public byte[] bytes;

		public BinaryCondition(string id = null) : base(id)
		{ }
		public BinaryCondition(XMLNode node) : base(node)
		{
			if (!node.HasValue)
				throw new Exception("BINARY must have text");
			string val = node.Value;
			if (val.Length % 2 != 0)
				throw new Exception("BINARY must contain bytes in hex. Current value contains an odd count of chars");
			this.bytes = Enumerable.Range(0, val.Length).Where(x => x % 2 == 0).Select(x => Convert.ToByte(val.Substring(x, 2), 16)).ToArray();
		}

		protected override bool InternalTest(File f)
		{
			byte[] content = f.ByteContent;
			byte[] searchContent = this.bytes;
			int len = content.Length;
			int lenSearch = this.bytes.Length;
			bool found = false;
			for (int i = 0; i < len; ++i)
			{
				found = true;
				for (int j = 0; j < lenSearch; ++j)
					if (content[i + j] != searchContent[j])
					{
						found = false;
						break;
					}
				if (found)
					return true;
			}
			return false;
		}

		[Register]
		public static void Register(Managers m)
		{
			m.cm.Register("bytes", (node, manager) => new BinaryCondition(node));
		}
	}

	public class JITCondition : ConditionBase
	{
		XMLNode innerCond;
		Managers m;

		public JITCondition(XMLNode n, Managers m) : base(n)
		{
			if(!n.HasSubNodes || n.SubNodes.Count != 1)
				throw new Exception($"{nameof(JITCondition)} must have exactly one subnode");
			this.innerCond = n.SubNodes[0];
			this.m = m;
		}

		protected override bool InternalTest(File f)
		{
			ICondition cond = this.m.cm.GetObject(this.innerCond);
			return cond.Test(f);
		}

		[Register]
		public static void Register(Managers m)
		{
			m.cm.Register("jitcond", (node, managers) => new JITCondition(node, managers));
		}
	}
}
