using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using XMLObjects;
using XPF;

namespace MXPF
{
	public class IfAction : ActionBase
	{
		public ICondition condition;
		public IAction thenAction;
		public IAction elseAction;

		public IfAction(string id = null) : base(id)
		{ }
		public IfAction(XMLNode node, Managers m) : base(node)
		{
			if(!node.HasSubNodes)
				throw new Exception("IF-Action must have SubNodes");
			XMLNode ifNodes = new XMLNode();
			bool ifSection = true;
			foreach(XMLNode subNode in node.SubNodes)
				if(ifSection)
					if(subNode.Name == "then")
					{
						this.thenAction = new XPF.Action(subNode, m);
						ifSection = true;
					}
					else
						ifNodes.SubNodes.Add(subNode);
				else if(subNode.Name != "else")
					throw new Exception("After a THEN only an ELSE is allowed");
				else if(this.elseAction != null)
					throw new Exception("After a ELSE no node is allowed");
				else
					this.elseAction = new XPF.Action(subNode, m);
			if(this.thenAction == null)
				throw new Exception("IF-Node must have a THEN");
			if(this.elseAction == null)
				this.elseAction = new EmptyAction();
			this.condition = new AndCondition(ifNodes, m);
		}

		public override void Run(XPF.File f)
		{
			if(this.condition.Test(f))
				this.thenAction.Run(f);
			else
				this.elseAction.Run(f);
		}

		[Register]
		public static void Register(Managers m)
		{
			m.am.Register("if", (node, managers) => new IfAction(node, managers));
		}
	}

	public class CopyAction : ActionBase
	{
		public bool overrideFile = false;

		public CopyAction()
		{ }
		public CopyAction(XMLNode node) : base(node)
		{
			if (node.Attributes.ContainsKey("override"))
				this.overrideFile = node.Attributes["override"].ToLower() == "true";
		}

		public override void Run(XPF.File f)
		{
			if(f.isDirectory)
			{
				Console.WriteLine($"Info: Creating folder '{f.DestinationPath}'");
				Directory.CreateDirectory(f.DestinationPath);
			}
			else
			{
				Console.WriteLine($"Info: Copying file '{f.RelativePath}' to '{f.DestinationPath}");
				bool exists = System.IO.File.Exists(f.DestinationPath);
				if(exists)
					Console.WriteLine($"Info: File '{f.DestinationPath}' exists. {(this.overrideFile ? "O" : "Won't o")}verriding it");
				if(!exists || this.overrideFile)
					System.IO.File.Copy(f.absolutePath, f.DestinationPath, this.overrideFile);
			}
		}

		[Register]
		public static void Register(Managers m)
		{
			m.am.Register("copy", (node, manager) => new CopyAction(node));
		}
	}

	public class WriteAction : ActionBase
	{
		public enum DataType
		{
			TEXT,
			BINARY,
			DYNAMIC
		}

		public Encoding encoding;
		public DataType dataType = DataType.DYNAMIC;

		public WriteAction()
		{ }
		public WriteAction(XMLNode node) : base(node)
		{
			if(node.HasAttr("datatype") && !Enum.TryParse(node["datatype"], true, out this.dataType))
				throw new Exception($"Invalid value '{node["datatype"]}' in datatype-attribute. Allowed are TEXT, BINARY");
			if (node.HasAttr("encoding"))
				if (this.dataType == DataType.BINARY)
					Console.WriteLine("Warning: encoding-attribute is ignored because the datatype is fixed to BINARY");
				else
					this.encoding = Encoding.GetEncoding(node["encoding"]);
		}

		public override void Run(XPF.File f)
		{
			if(f.isDirectory)
				Directory.CreateDirectory(f.DestinationPath);
			else
				switch(this.dataType)
				{
					case DataType.DYNAMIC:
						if(f.HasTextContent && f.HasBinaryContent)
							Console.WriteLine("Warning: File has text and binary content stored in RAM. Writing binary content");
						if(f.HasBinaryContent)
							System.IO.File.WriteAllBytes(f.DestinationPath, f.ByteContent);
						else if(f.HasTextContent)
							WriteText(f);
						break;
					case DataType.BINARY:
						System.IO.File.WriteAllBytes(f.DestinationPath, f.ByteContent);
						break;
					case DataType.TEXT:
						WriteText(f);
						break;
				}
		}

		void WriteText(XPF.File f)
		{
			if (this.encoding == null)
				Console.WriteLine("Warning: No encoding specified. Using UTF-8");
			System.IO.File.WriteAllText(f.DestinationPath, f.TextContent, this.encoding ?? Encoding.UTF8);
		}

		[Register]
		public static void Register(Managers m)
		{
			m.am.Register("write", (node, manager) => new WriteAction(node));
		}
	}

	public class OutAction : ActionBase
	{
		public string text;

		public OutAction()
		{ }
		public OutAction(XMLNode node) : base(node)
		{
			if (!node.HasValue)
				throw new Exception("OUT must contain text");
			this.text = node.Value;
		}

		public override void Run(XPF.File f)
		{
			Console.WriteLine(string.Format(this.text));
		}


		[Register]
		public static void Register(Managers m)
		{
			m.am.Register("out", (node, managers) => new OutAction(node));
		}
	}

	public class PathAction : ActionBase
	{
		string newPath;

		public PathAction()
		{ }
		public PathAction(XMLNode node) : base(node)
		{
			if (!node.HasValue)
				throw new Exception("PATH must contain a value");
			this.newPath = node.Value;
		}

		public override void Run(XPF.File f)
		{
			f.relativeDir = this.newPath;
		}

		[Register]
		public static void Register(Managers m)
		{
			m.am.Register("path", (node, managers) => new PathAction(node));
		}
	}

	public class SetLinesAction : ActionBase
	{
		public Regex r;
		public string replacement;
		public string lineEnd;
		public bool regexLineEnd;
		public string[] sourceIDs;

		public SetLinesAction()
		{ }
		public SetLinesAction(XMLNode node) : base(node)
		{
			if (!node.HasValue && !node.HasSubNodes)
				throw new Exception("SETLINES must contain a value or the subnodes REGEX and REPLACEMENT");
			if (!node.HasAttr("from"))
				throw new Exception("SETLINES must contain FROM-attribute");
			this.sourceIDs = node.Attributes["from"].Split(' ');
			if (node.HasValue)
				this.replacement = node.Value;
			else
			{
				if (!node.HasSubNode("regex") || !node.HasSubNode("replacement"))
					throw new Exception("NAME must have a value or the subnodes REGEX and REPLACEMENT");
				XMLNode subNode = node.GetSubNode("regex");
				if (!subNode.HasValue)
					throw new Exception("NAME -> REGEX must have a value");
				this.r = new Regex(subNode.Value, RegexOptions.Compiled);
				subNode = node.GetSubNode("replacement");
				if (!subNode.HasValue)
					throw new Exception("NAME -> REPLACEMENT must have a value");
				this.replacement = subNode.Value;
			}
			if (node.HasAttr("lineending"))
			{
				this.lineEnd = node["lineending"];
				if (this.r != null && this.lineEnd == "regex")
					this.regexLineEnd = true;
			}
		}

		public override void Run(XPF.File f)
		{
			List<FoundLines> lines = new List<FoundLines>();
			Dictionary<string, object> storedObjects = f.GetStoredObjects(this.sourceIDs);
			foreach (KeyValuePair<string, object> obj in storedObjects)
				if (obj.Value is FoundLines)
					lines.Add((FoundLines)obj.Value);
				else
					throw new Exception("Object with key '" + obj.Key + "' is not an instance of '" + nameof(FoundLines) + "'");
			string[] seperator = null;
			foreach(FoundLines line in lines)
				if (seperator == null)
					seperator = line.lineSeperators;
				else if (!Enumerable.SequenceEqual(line.lineSeperators, seperator))
					Console.WriteLine("Warning: Different line seperators. Result might be unexpected");

			if (f.textContentChanged)
				Console.WriteLine("Warning: Text content was changed since last line selection. You should do a redo first. Otherwise the results might be unexpected");

			List<string> splitted;
			StringBuilder merged;
			if (!this.regexLineEnd)
			{
				splitted = f.TextContent.ExtendedSplit(seperator);
				if (this.r == null)
					foreach (FoundLines line in lines)
						foreach (int i in line.foundLines)
							splitted[i * 2] = this.replacement;
				else
					foreach (FoundLines line in lines)
						foreach (int i in line.foundLines)
							this.r.Replace(splitted[i * 2], this.replacement);
				if (this.lineEnd != null)
					foreach (FoundLines line in lines)
						foreach (int i in line.foundLines)
							splitted[i * 2 + 1] = this.lineEnd;
				merged = new StringBuilder();
				for (int i = 0; i < splitted.Count; i += 2)
				{
					if (splitted[i] == null)
						continue;
					merged.Append(splitted[i]);
					merged.Append(splitted[i + 1]);
				}
			}
			else
			{
				splitted = f.TextContent.ExtendedSplit(seperator, false);
				foreach (FoundLines line in lines)
					foreach (int i in line.foundLines)
						this.r.Replace(splitted[i], this.replacement);
				merged = new StringBuilder();
				foreach (string s in splitted)
					merged.Append(s);
			}
			f.TextContent = merged.ToString();
		}

		[Register]
		public static void Register(Managers m)
		{
			m.am.Register("setlines", (node, managers) => new SetLinesAction(node));
		}
	}

	public static class ChangedNames
	{
		public static Dictionary<string, string> changedNames = new Dictionary<string, string>();
	}

	public class NameAction : ActionBase
	{
		public string newName;

		public Regex r;
		public string replacement;

		public NameAction(string id = null) : base(id)
		{ }

		public NameAction(XMLNode node) : base(node)
		{
			if (!node.HasValue && !node.HasSubNodes)
				throw new Exception("NAME must have a value or the subnodes REGEX and REPLACEMENT");
			if (node.HasValue)
				this.newName = node.Value;
			else
			{
				if(!node.HasSubNode("regex") || !node.HasSubNode("replacement"))
					throw new Exception("NAME must have a value or the subnodes REGEX and REPLACEMENT");
				XMLNode subNode = node.GetSubNode("regex");
				if (!subNode.HasValue)
					throw new Exception("NAME -> REGEX must have a value");
				this.r = new Regex(subNode.Value, RegexOptions.Compiled);
				subNode = node.GetSubNode("replacement");
				if (!subNode.HasValue)
					throw new Exception("NAME -> REPLACEMENT must have a value");
				this.replacement = subNode.Value;
			}
		}

		public override void Run(XPF.File f)
		{
			if (this.newName != null)
				f.Name = this.newName;
			else
				f.Name = this.r.Replace(f.Name, this.replacement);
		}

		[Register]
		public static void Register(Managers m)
		{
			m.am.Register("name", (node, managers) => new NameAction(node));
		}
	}

	public class MinifyAction : ActionBase
	{
		public enum MinifyMode
		{
			LOCAL,
			REMOTE
		}

		public enum MinifyType
		{
			JS,
			CSS
		}

		public MinifyType type;
		public MinifyMode mode = MinifyMode.LOCAL;

		public MinifyAction(string id = null) : base(id)
		{ }
		public MinifyAction(XMLNode node) : base(node)
		{
			if (node.HasValue || node.HasSubNodes)
				throw new Exception("MINIFY mustn't contain a value or subnodes");
			if (!node.HasAttr("type"))
				throw new Exception("MINIFY must have the attribute TYPE");
			if (!Enum.TryParse(node["type"], true, out this.type))
				throw new Exception("MINIFY-TYPE must be JS or CSS");
			if(node.HasAttr("mode"))
				if(!Enum.TryParse(node["mode"], true, out this.mode))
					throw new Exception("MINIFY-MODE must be LOCAL or REMOTE");
		}


		public override void Run(XPF.File f)
		{
			string content = null;
			switch(this.mode)
			{
				case MinifyMode.LOCAL:
					content = MinifyLocal(f);
					break;
				case MinifyMode.REMOTE:
					content = MinifyRemote(f);
					break;
			}
			if (content == null)
				Console.WriteLine("Error: Unknown error while minifying");
			else
				f.TextContent = content;
		}

		public string MinifyLocal(XPF.File f)
		{
			switch(this.type)
			{
				case MinifyType.CSS:
					return Minifier.MinifyCSS(f.TextContent);
				case MinifyType.JS:
					return Minifier.MinifyJS(f.TextContent);
			}
			return null;
		}
		public string MinifyRemote(XPF.File f)
		{
			string input = WebUtility.UrlEncode(f.TextContent);
			switch(this.type)
			{
				case MinifyType.CSS:
					return Fetch("http://cssminifier.com/raw", input);
				case MinifyType.JS:
					return Fetch("http://javascript-minifier.com/raw", input);
			}
			return null;
		}

		public string Fetch(string url, string input)
		{
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
			request.Method = "POST";
			string formContent = "input=" + input;
			byte[] byteArray = Encoding.UTF8.GetBytes(formContent);
			request.ContentType = "application/x-www-form-urlencoded";
			request.ContentLength = byteArray.Length;

			Stream str = null;
			Stream resStr = null;
			try
			{
				str = request.GetRequestStream();
				str.Write(byteArray, 0, byteArray.Length);
				str.Flush();

				WebResponse response = request.GetResponse();
				resStr = response.GetResponseStream();

				string content = null;
				if(str != null)
				{
					StreamReader reader = new StreamReader(resStr);
					content = reader.ReadToEnd();
					reader.Close();
					resStr.Close();
				}
				response.Close();
				return content;
			}
			finally
			{
				str?.Close();
				resStr?.Close();
			}
		}

		[Register]
		public static void Register(Managers m)
		{
			m.am.Register("minify", (node, manager) => new MinifyAction(node));
		}
	}

	public class JITAction : ActionBase
	{
		XMLNode innerAction;
		Managers m;

		public JITAction(XMLNode n, Managers m) : base(n)
		{
			if(!n.HasSubNodes || n.SubNodes.Count != 1)
				throw new Exception($"{nameof(JITAction)} must have exactly one subnode");
			this.innerAction = n.SubNodes[0];
			this.m = m;
		}

		public override void Run(XPF.File f)
		{
			this.m.am.GetObject(this.innerAction).Run(f);
		}

		[Register]
		public static void Register(Managers m)
		{
			m.am.Register("jitaction", (node, managers) => new JITAction(node, managers));
		}
	}
}
