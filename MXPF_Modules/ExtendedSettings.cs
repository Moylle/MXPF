using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XMLObjects;
using XPF;

namespace MXPF
{
	public class Settings : SettingBase
	{
		public List<ISetting> settings = new List<ISetting>();

		public Settings(string id = null) : base(id)
		{ }
		public Settings(XMLNode node, Managers m) : base(node)
		{
			if (node.HasValue)
				throw new Exception("SETTINGS mustn't have a value");
			foreach (XMLNode subNode in node.SubNodes)
				this.settings.Add(m.sm.GetObject(subNode));
		}

		public override void Apply()
		{
			foreach (ISetting setting in this.settings)
				setting.Apply();
		}

		[Register]
		public static void Register(Managers m)
		{
			m.sm.Register("settings", (node, managers) => new Settings(node, managers));
		}
	}

	public abstract class DirSetting : SettingBase
	{
		public string directory;
		public Managers m;

		public DirSetting(string id = null) : base(id)
		{ }
		public DirSetting(XMLNode node, Managers m) : base(node)
		{
			if (!node.HasValue)
				throw new Exception($"{GetType().Name} must have a value");
			if (!Directory.Exists(node.Value))
				throw new Exception($"The directory '{node.Value}' doesn't exist");
			this.directory = node.Value;
			this.m = m;
		}
	}

	public class SrcDirSetting : DirSetting
	{
		public SrcDirSetting(string id = null) : base(id)
		{ }
		public SrcDirSetting(XMLNode node, Managers m) : base(node, m)
		{ }

		public override void Apply()
		{
			this.m.xm.SrcDir = this.directory;
		}

		[Register]
		public static void Register(Managers m)
		{
			m.sm.Register("srcdir", (node, managers) => new SrcDirSetting(node, managers));
		}
	}

	public class DstDirSetting : DirSetting
	{
		public DstDirSetting(string id = null) : base(id)
		{ }

		public DstDirSetting(XMLNode node, Managers m) : base(node, m)
		{ }

		public override void Apply()
		{
			this.m.xm.DstDir = this.directory;
		}

		[Register]
		public static void Register(Managers m)
		{
			m.sm.Register("dstdir", (node, managers) => new DstDirSetting(node, managers));
		}
	}

	public class LogSetting : SettingBase
	{
		string path;

		public LogSetting(XMLNode node) : base(node)
		{
			if(!node.HasValue)
				throw new Exception("LOG must have a value");
			this.path = node.Value;
		}

		public override void Apply()
		{
			Logger.SetLogFile(this.path);
		}

		[Register]
		public static void Register(Managers m)
		{
			m.sm.Register("log", (node, managers) => new LogSetting(node));
		}
	}

	public class EncodingSetting : SettingBase
	{
		Encoding e;

		public EncodingSetting(XMLNode node) : base(node)
		{
			if(!node.HasValue)
				throw new Exception("ENCODING must have a value");
			try
			{
				e = Encoding.GetEncoding(node.Value);
			}
			catch
			{
				throw new Exception($"Unknown encoding '{node.Value}'");
			}
		}

		public override void Apply()
		{
			XPF.File.Encoding = this.e;
			Console.WriteLine($"Info: Changed encoding to '{this.e.EncodingName}'");
		}

		[Register]
		public static void Register(Managers m)
		{
			m.sm.Register("encoding", (node, managers) => new EncodingSetting(node));
		}
	}
}
